# Wunderman Thompson Vue.js Test

## Project setup
```
git clone https://gitlab.com/ankit20893/wunderman-test.git
```
```
cd wunderman-test
```
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
